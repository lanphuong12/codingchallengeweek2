package Week2Day5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class Day5 {

	public static void main(String[] args) {
		// q1
		ArrayList<String> fruits = new ArrayList<>();

		try {
			checkFruit("Apple", fruits);
			checkFruit("Mango", fruits);
			checkFruit("Mango", fruits);
		} catch (customEx e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		// q2
		HashMap<String, Integer> map = new HashMap<>();
		HashSet<String> h = new HashSet<String>();

		map.put("Apple HashMap", 10);
		map.put("Mango HashMap", 30);
		map.put("Mango HashMap", 20);

		h.add("Apple");
		h.add("Orange");
		h.add("Cherry");

		for (String name : fruits) {
			System.out.println(name);
		}

		Iterator<String> i = h.iterator();
		while (i.hasNext())
			System.out.println(i.next());

		for (HashMap.Entry<String, Integer> entry : map.entrySet())
			System.out.println("Fruit Name = " + entry.getKey() + ", Value = " + entry.getValue());
	}

	public static void checkFruit(String name, ArrayList<String> a) throws customEx {
		if (a.contains(name)) {
			// throw new customEx();
		} else {
			a.add(name);
			System.out.println(name + " is added to the ArrayList");
		}
	}

}
