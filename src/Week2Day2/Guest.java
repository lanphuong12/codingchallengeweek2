package Week2Day2;

public class Guest {

	public static void main(String[] args) throws InterruptedException {
//		String name1 = "guest1";
//		String name2 = "guest2";
//		String name3 = "guest3";
		Thread t1 = new Thread(new Greet("guest1"));
		Thread t2 = new Thread(new Greet("guest2"));
		Thread t3 = new Thread(new Greet("guest3"));

//		Greet t1 = new Greet("guest1");
//		Greet t2 = new Greet("guest2");
//		Greet t3 = new Greet("guest3");
		t1.start();
		t1.join();
		t2.start();
		t2.join();
		t3.start();
	}
}

class Greet extends Thread {
	String name;

	Greet(String name) {
		this.name = name;
	}

	void printGreeting(String guestName) {

		try {
			System.out.println("Welcome " + guestName);
			Thread.sleep(250);
			System.out.println("How are you doing, " + guestName + "?");
			Thread.sleep(250);
			System.out.println("Goodbye for now, see you soon " + guestName);
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		printGreeting(this.name);
	}

}