package CodingChallengeWeek2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class CodingChallengeWeek2 extends Thread{

	static UserImpl userImpl = new UserImpl();
	
	static ArrayList<UserAtributes> arrUsers;
 	static ArrayList<BookAttributes> arrBooks;

 	
	@Override
	public void run() {
		Login(this.arrUsers, this.arrBooks);
		
	}

	public static void Login(ArrayList<UserAtributes> user, ArrayList<BookAttributes> arrBook) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcom to my project! Please log in first!");
		boolean tt = true;
		do {
			System.out.print("Username: ");
			String userString = sc.nextLine();
			System.out.print("Password: ");
			String passString = sc.nextLine();
			for (int i = 0; i < user.size(); i++) {
				if (userString.equals(user.get(i).getUserName()) && passString.equals(user.get(i).getPassword())) {
					userImpl.LogFile(userString + " login");
					userImpl.Menu(arrBook, user, userString);
				} else {
					tt = false;
				}
			}
			if (tt == false) {
				System.out.println("I can't find your account. Please try again!");
			}
		} while (tt == false);
	}

	public ArrayList<UserAtributes> ReadFileUser(ArrayList<UserAtributes> ListUser) {
		String filename = "C:/Week2/DataWeek2.txt";
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}

			try (BufferedReader bReader = new BufferedReader(new FileReader(filename))) {
				String lineString = bReader.readLine();
				while (lineString != null) {
					UserAtributes userAtributes = new UserAtributes(lineString);
					ListUser.add(userAtributes);
					lineString = bReader.readLine();
				}
			}
		} catch (Exception e) {
		}
		return ListUser;
	}

	public ArrayList<BookAttributes> ReadFileBook(ArrayList<BookAttributes> ListBook) {
		String filename = "C:/Week2/DataBookWeek2.txt";
		try {
			File file = new File(filename);
			if (!file.exists()) {
				file.createNewFile();
			}

			try (BufferedReader bReader = new BufferedReader(new FileReader(filename))) {
				String lineString = bReader.readLine();
				while (lineString != null) {
					BookAttributes bookAttributes = new BookAttributes(lineString);
					ListBook.add(bookAttributes);
					lineString = bReader.readLine();
				}
			}
		} catch (Exception e) {
		}
		return ListBook;
	}
	
	public static void main(String[] args) throws SecurityException, IOException {

		CodingChallengeWeek2 week2 = new CodingChallengeWeek2();
		ArrayList<BookAttributes> arrBookAttributes = new ArrayList<>();
		arrBookAttributes = week2.ReadFileBook(arrBookAttributes);
		arrBooks = arrBookAttributes;
		
		ArrayList<UserAtributes> arruserAtributes = new ArrayList<>();
		arruserAtributes = week2.ReadFileUser(arruserAtributes);
		arrUsers = arruserAtributes;
		userImpl.LogFile("Running");
		week2.start();
		//Login(arruserAtributes, arrBookAttributes);
	}

}
